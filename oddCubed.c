//Считать с клавиатуры целые числа min и max. Вывести в строку кубы всех нечетных чисел в промежутке от min до max включительно. В указаном промежутке гарантированно существует минимум одно нечетное число.

//Пример ввода
//0 7
//Пример вывода
//1 27 125 343

#include <stdio.h>

int main() {
    int min, max, cube, border;
    
    scanf("%d %d", &min, &max);
    
    border = max - 1;
    for ( int counter = min; counter < border; counter++ ) {
        if ( counter % 2 != 0 ) {
            cube = counter * counter * counter;
            printf("%d ", cube);
        }
    }
    if ( border % 2 != 0 ) {
        cube = border * border * border;
        printf("%d\n", cube);
    } else {
        cube = max * max * max;
        printf("%d\n", cube);
    }
    
    return 0;
}

