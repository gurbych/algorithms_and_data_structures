//Считать с клавиатуры числовую последовательность - 5 целых чисел. Проверить, упорядочена ли последовательность по неубыванию. Если упорядочена вывести на экран «yes», в протиивном случае вывести «no». Задача решается без использования циклов.

//Пример ввода
//-20 -15 0 12 15
//Пример вывода
//yes

#include <stdio.h>

int main() {
    int current, next;
    
    scanf("%d", &current);
    for ( int counter = 0; scanf("%d", &next) == 1 && counter < 5; counter++, current = next ) {
        if ( next < current ) {
            printf("no\n");
            return 0;
        }
    }
    printf("yes\n");
    return 0;
}

