//Написать функцию:
//int isPrime(int n)
//Проверить, является ли данное число простым.

int isPrime(int n) {
    if ( n <= 1 ) {
        return 0;
    }
    for ( int counter = 2; counter < n; counter++ ) {
        if ( n % counter == 0 ) {
            return 0;
        }
    }
    
    return 1;
}

