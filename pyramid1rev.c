//Вывести числовую пирамидку на total строк. Порядок строк – обратный.
//В каждой строке числа идут от единицы до номера строки через пробел.

//Пример ввода
//3
//Пример вывода
//1 2 3
//1 2
//1

#include <stdio.h>

int main() {
    int total;
    
    scanf("%d", &total);
    
    for ( ; total >= 1; total-- ) {
        for ( int col = 1; col < total; col++ ) {
            printf("%d ", col);
        }
        printf("%d\n", total);
    }
    
    return 0;
}

