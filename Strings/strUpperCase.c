void strUpperCase(char str[]) {
    for ( int i = 0, symbol = str[0]; symbol != '\0'; i++ ) {
        symbol = str[i];
        if ( symbol >= 'a' && symbol <= 'z' ) {
            symbol += 'A' - 'a';
        }
        str[i] = symbol;
    }
}

