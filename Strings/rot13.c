//Считать из файла task.in в память строку длиной не более 100 символов.
//Зашифровать её по алгоритму ROT13.
//Результат вывести в task.out.

#include <stdio.h>

#define SIZE 100

void strRot13(char str[]) {
    for ( int i = 0; str[i] != '\0'; i++ ) {
        char current = str[i];
        
        if ( current >= 'a' ) {
            if ( current <= 'm' ) {
                current += 13;
            } else if ( current <= 'z' ) {
                current -= 13;
            }
        } else if ( current >= 'A' ) {
            if ( current <= 'M' ) {
                current += 13;
            } else if ( current <= 'Z' ) {
                current -= 13;
            }
        }
        str[i] = current;
    }
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    char str[SIZE+1];
    
    fscanf(in, "%100s", str);
    strRot13(str);
    fprintf(out, "%s\n", str);
    return 0;
}

