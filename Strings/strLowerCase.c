void strLowerCase(char str[]) {
    for ( int i = 0, symbol = str[0]; symbol != '\0'; i++ ) {
        symbol = str[i];
        if ( symbol >= 'A' && symbol <= 'Z' ) {
            symbol += 'a' - 'A';
        }
        str[i] = symbol;
    }
}

