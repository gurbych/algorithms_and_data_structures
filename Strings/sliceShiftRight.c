//циклический сдвиг части массива на один элемент вправо от start до end включительно.

void sliceShiftRight(int array[], int start, int end) {
    if ( end - start > 0 ) {
        int temp = array[end];
        
        for ( ; start < end; end-- ) {
            array[end] = array[end-1];
        }
        array[start] = temp;
    }
}

