//Осуществить циклический сдвиг части массива на один элемент влево от start до end включительно.

void sliceShiftLeft(int array[], int start, int end) {
    if ( end - start > 0 ) {
        int temp = array[start];
        
        for ( ; start < end; start++ ) {
            array[start] = array[start+1];
        }
        array[end] = temp;
    }
}

