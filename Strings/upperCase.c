//В файле task.in дана последовательность символов.
//Перевести все буквы в верхний регистр и вывести результирующую последовательность в файл task.out

#include <stdio.h>

#define DIF 32

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    char current;
    
    for ( int i = 0; fscanf(in, "%c", &current) == 1; i++ ) {
        if ( current >= 'a' && current <= 'z' ) {
            current -= DIF;
        }
        fprintf(out, "%c", current);
    }
    fprintf(out, "\n");
    
    return 0;
}

