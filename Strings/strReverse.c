//В файле task.in дана строка длиной не более 100 символов.
//Прочитать строку в память и произвести ее реверс.
//Результат вывести в task.out.

//Пример ввода
//hello
//Пример вывода
//olleh

#include <stdio.h>

#define SIZE 100

int strLen(char str[]) {
    int length = 0;
    
    for ( ; str[length] != '\0'; length++ );
    return length;
}

void strReverse(char str[], int last) {
    for ( int i = 0, j = last; i < j; i++, j-- ) {
        char temp = str[i];
        
        str[i] = str[j];
        str[j] = temp;
    }
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    char str[SIZE+1];
    int last;
    
    fscanf(in, "%100s", str);
    last = strLen(str) - 1;
    strReverse(str, last);
    fprintf(out, "%s\n", str);
    fclose(in);
    fclose(out);
    
    return 0;
}

