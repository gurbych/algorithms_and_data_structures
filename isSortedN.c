//Считать с клавиатуры положительную длину числовой последовательности и саму последовательность. Проверить, упорядочена ли последовательность по неубыванию. Если упорядочена вывести на экран «yes», в противном случае вывести «no».

//Пример ввода
//5
//-20 -15 0 12 15
//Пример вывода
//yes

#include <stdio.h>

int main() {
    int sequenceLength, firstMember, nextMember;
    
    scanf("%d", &sequenceLength);
    scanf("%d", &firstMember);
    
    for ( int counter = 1; counter < sequenceLength; counter++ ) {
        scanf("%d", &nextMember);
        if ( nextMember < firstMember ) {
            printf("no\n");
            return 0;
        }
        firstMember = nextMember;
    }
    printf("yes\n");
    
    return 0;
}

