#include <stdio.h>

int sum(int a, int b) {
    return a + b;
}

int main() {
    int (*my_func)(int,int);

    my_func = &sum;

    printf("%d\n", sum(5, 42));
    printf("%d\n", my_func(10, 11));
    printf("%d\n", (*my_func)(1, 2));

    return 0;
}
