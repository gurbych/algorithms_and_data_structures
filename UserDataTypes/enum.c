#include <stdio.h>

// enum days {
//     MONDAY = 1,
//     TUESDAY = 100500,
//     WEDNESDAY = 42,
//     THURSDAY = -1000000,
//     FRIDAY = 0,
//     SATURDAY = 666,
//     SUNDAY = 5,
// };

// enum {
//     FALSE,
//     TRUE
// };

union smth {
   int a;
   double z;
} smth;

int main() {
    smth.a = 5;

    printf("%d\n", smth.a);
    printf("%f\n", smth.z);

    smth.z = 12.08;

    printf("%f\n", smth.z);
    printf("%d\n", smth.a);


    // MONDAY += 1;

    // printf("%d\n", MONDAY);
    // printf("%d\n", TUESDAY);
    // printf("%d\n", WEDNESDAY);
    // printf("%d\n", THURSDAY);
    // printf("%d\n", FRIDAY);
    // printf("%d\n", SATURDAY);
    // printf("%d\n", SUNDAY);

    // printf("%lu\n", sizeof(MONDAY));

    return 0;
}
