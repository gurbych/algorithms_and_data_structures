#include <stdio.h>

typedef struct {
    int day;
    int month;
    int year;
    char name[50];
    char lastName[50];
    char middleName[50];
} Human;


void humanPrint(Human human) {
    printf("%s %s %s ", human.name, human.middleName, human.lastName);
    printf("was born on %d.%d.%d\n", human.day, human.month, human.year);
}

Human makeSiameseTwins(Human a, Human* b) {
    Human temp;

    scanf("%s", temp.name);
    scanf("%s", temp.lastName);
    scanf("%s", temp.middleName);
    temp.day = a.day + b->day;
    temp.month = a.month + b->month;
    temp.year = a.year + b->year;

    return temp;
}

int main() {
    Human john = {10, 10, 1990, "John", "Smith", "H"};
    Human alex = {10, 10, 1990, "Alex", "Smith", "H"};

    Human electionList[5];

    electionList[0] = alex;
    electionList[1] = john;

    Human siameseTwins = makeSiameseTwins(alex, &john);

    humanPrint(john);

    humanPrint(siameseTwins);


    // char human[200] = {"John H Smith 10.10.1990"};

    // scanf("%s", name);
    // scanf("%s", lastName);
    // scanf("%s", middleName);
    // scanf("%d", &day);
    // scanf("%d", &month);
    // scanf("%d", &year);

    // printf("%s %s %s was born on %d.%d.%d\n", name, middleName, lastName, day, month, year);
    // printf("%s\n", human);

    return 0;
}
