//В файле task.in дано число len и массив размером len элементов.
//Отсортировать элементы в порядке неубывания.
//Использовать алгоритм quickSort.
//Результат записать в task.out

#include <stdio.h>

int getLen(FILE *in) {
    int len;
    
    fscanf(in, "%d", &len);
    return len;
}

void arrayRead(FILE *in, int array[], int size) {
    for ( int i = 0; i < size; i++ ) {
        fscanf(in, "%d", &array[i]);
    }
}

void arrayPrint(FILE *out, int array[], int size) {
    int last = size - 1;
    
    for ( int i = 0; i < last; i++ ) {
        fprintf(out, "%d ", array[i]);
    }
    fprintf(out, "%d\n", array[last]);
}

int partition(int array[], int lo, int hi) {
    int avg = (lo + hi) / 2;
    int pivot = array[avg];
    int tail = lo;
    
    array[avg] = array[hi];
    array[hi] = pivot;
    
    for ( ; array[tail] < array[hi]; tail++ );
    for ( int i = tail + 1; i < hi; i++ ) {
        if ( array[i] < array[hi] ) {
            pivot = array[i];
            array[i] = array[tail];
            array[tail] = pivot;
            tail += 1;
        }
    }
    
    pivot = array[tail];
    array[tail] = array[hi];
    array[hi] = pivot;
    
    return tail;
}

void quickSort(int array[], int lo, int hi) {
    int pivot;
    
    if ( lo < hi ) {
        pivot = partition(array, lo, hi);
        quickSort(array, lo, pivot-1);
        quickSort(array, pivot+1, hi);
    }
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int hi = getLen(in);
    int array[hi];
    
    arrayRead(in, array, hi);
    
    quickSort(array, 0, hi-1);
    
    arrayPrint(out, array, hi);
    
    fclose(in);
    fclose(out);
    
    return 0;
}

