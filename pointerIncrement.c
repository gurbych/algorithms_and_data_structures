//Увеличить на единицу значение по переданному адресу value.

#include <stdio.h>
 
void pointerIncrement(int *value) {
    *value += 1;
}
 
int main () {
    int value = 10;
 
    printf("%d\n", value);
    pointerIncrement(&value);
    printf("%d\n", value);
     
    return 0;
}

