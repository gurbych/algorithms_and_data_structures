//В качестве опорного элемента брать среднее арифметическое между start и end с округлением вниз.

int partition(int array[], int lo, int hi) {
    int avg = (lo + hi) / 2;
    int pivot = array[avg];
    int tail = lo;
    
    array[avg] = array[hi];
    array[hi] = pivot;
    
    for ( ; array[tail] < array[hi]; tail++ );
    for ( int i = tail + 1; i < hi; i++ ) {
        if ( array[i] < array[hi] ) {
            pivot = array[i];
            array[i] = array[tail];
            array[tail] = pivot;
            tail += 1;
        }
    }
    
    pivot = array[tail];
    array[tail] = array[hi];
    array[hi] = pivot;
    
    return tail;
}

