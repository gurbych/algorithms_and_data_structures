//Вывести числовой квадрат заданного размера.
//Выведенные числа начинаются с единицы и постоянно увеличиваются.
//В каждой строке числа разделены пробелами.
//Порядок строк обратный.
//Размер считать с клавиатуры.

//Пример ввода
//2
//Пример вывода
//3 4
//1 2

#include <stdio.h>

int main() {
    int lastElement, total, counter;
    
    scanf("%d", &total);
    lastElement = total * total;
    for ( int row = 1; row <= total; row++ ) {
        lastElement -= total;
        counter = lastElement;
        
        for ( int col = 1; col < total; col++ ) {
            lastElement += 1;
            printf("%d ", lastElement);
        }
        lastElement += 1;
        
        printf("%d\n", lastElement);
        
        lastElement = counter;
    }
    
    return 0;
}

