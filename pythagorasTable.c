//Нарисовать таблицу Пифагора (умножения) с форматированием и разметкой для заданных диапазонов по горизонтали и вертикали.
//Расстояние от края экрана до чисел в заголовке строк - не менее одного пробела.
//Расстояние от вертикальной черты до чисел - не менее одного пробела.
//Расстояние между числами в таблице - не менее одного пробела.
//Ширина всех колонок должна быть одинаковой.
//В вопросе расставления пробелов руководствуйтесь здравым смыслом и примером.

//Пример ввода
//7 11 7 11
//Пример вывода
//    |   7   8   9  10  11
//----+--------------------
//  7 |  49  56  63  70  77
//  8 |  56  64  72  80  88
//  9 |  63  72  81  90  99
// 10 |  70  80  90 100 110
// 11 |  77  88  99 110 121

#include <stdio.h>

int spaces(int n) {
    int space = 0;
    
    if ( n < 0 ) {
        n *= -1;
        space += 1;
    }
    for ( ; n > 0; space++ ) {
        n /= 10;
    }
    return space + 1;
}

int main() {
    int min1, max1, min2, max2, maxSpace1, maxSpace2;
    char c = '|';
    
    scanf("%d %d %d %d", &min1, &max1, &min2, &max2);
    
    maxSpace1 = spaces(min2);
    if ( spaces(max2) > spaces(min2) ) {
        maxSpace1 = spaces(max2);
    }
    
    maxSpace2 = spaces(min2*min1);
    if ( spaces(max2*max1) > spaces(min2*min1) ) {
        maxSpace2 = spaces(max2*max1);
    }
    
    printf("%*c", maxSpace1+2, c);
    for ( int i = min1; i <= max1; i++ ) {
        printf("%*d", maxSpace2, i);
    }
    printf("\n");
    
    for ( int i = 0; i <= maxSpace1; i++ ) {
        printf("-");
    }
    printf("+");
    for ( int i = min1; i <= max1; i++ ) {
        for ( int j = 0; j < maxSpace2; j++ ) {
            printf("-");
        }
    }
    printf("\n");
    
    for ( int i = min2; i <= max2; i++ ) {
        printf("%*d |", maxSpace1, i);
        for ( int j = min1; j <= max1; j++ ) {
            printf("%*d", maxSpace2, j*i);
        }
        printf("\n");
    }
    
    return 0;
}

