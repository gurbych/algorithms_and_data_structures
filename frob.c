//Ввести положительную длину последовательности чисел, затем саму последовательность.
//Вывести искаженную последовательность, полученную применением к исходным членам побитовой операции xor с числом 42.
//Каждый член вывести на отдельной строке.
//Пример ввода
//2
//0 42
//Пример вывода
//42
//0

#include <stdio.h>

int main() {
    int length, current;
    
    scanf("%d", &length);
    
    for ( int counter = 0; counter < length; counter++ ) {
        scanf("%d", &current);
        current ^= 42;
        printf("%d\n", current);
    }
    
    return 0;
}

