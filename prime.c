//Вывести N-ное по счёту простое число (см. Википедию).
//N считать с клавиатуры.
//Оптимизировать работу программы, насколько это возможно.
//В случае неопределенности ответа вывести -1.

//Пример ввода
//3
//Пример вывода
//5

#include <stdio.h>

int isPrime(int i) {
    for ( int counter = 2; counter < i; counter++ ) {
        if ( i % counter == 0 ) {
            return 0;
        }
    }
    return 1;
}

int main() {
    int n;
    int prime;
    
    scanf("%d", &n);
    
    if ( n < 1 ) {
        printf("-1\n");
        return 0;
    } else if ( n == 1 ) {
        printf("2\n");
        return 0;
    } else if ( n == 2 ) {
        printf("3\n");
        return 0;
    }
    for ( int i = 5, counter = 2; counter < n; i += 2 ) {
        if ( isPrime(i) ) {
            prime = i;
            counter += 1;
        }
    }
    printf("%d\n", prime);
    
    return 0;
}

