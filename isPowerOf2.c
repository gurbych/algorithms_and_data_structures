//Считать с клавиатуры целое положительное число. Если число является целой степенью двойки, вывести "yes", иначе вывести "no".

#include <stdio.h>

int main() {
    int n;
    
    scanf("%d", &n);
    
    if ( n & (n - 1) ) {
        printf("no\n");
    } else {
        printf("yes\n");
    }
    return 0;
}

