//Считать с клавиатуры целое положительное число - количество чисел в последовательности.
//Затем считать с клавиатуры заданное количество чисел и вывести наименьшее из них.
//Данная задача решается без массивов.

//Пример ввода
//5
//1 2 3 4 5
//Пример вывода
//1

#include <stdio.h>

int main() {
    int length, min, next;
    
    scanf("%d %d", &length, &min);
    
    for ( int counter = 1; counter < length; counter++ ) {
        scanf("%d", &next);
        if ( min > next ) {
            min = next;
        }
    }
    printf("%d\n", min);
    
    return 0;
}

