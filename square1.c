//Вывести числовой квадрат заданного размера.
//В каждой строке числа идут с единицы через пробел.
//Размер считать с клавиатуры.

//Пример ввода
//2
//Пример вывода
//1 2
//1 2

#include <stdio.h>

int main() {
    int total;
    
    scanf("%d", &total);
    
    for ( int row = 0; row < total; row++ ) {
        for ( int col = 1; col < total; col++ ) {
            printf("%d ", col);
        }
        printf("%d\n", total);
    }
    
    return 0;
}

