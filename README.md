# BOOTCAMP #

## (basic algorithms and data structures best practices) ##

**IO** (matrixScan.c, matrixPrint.c)

**Arithmetic operators** (pythagorasTable.c)

**Comparison operators** (maxN.c, minN.c)

**Conditional operations** (if, if-else, if-else if)

**Loops** (pyramid.c, square.c, pascalTriangle.c)

**Functions** (isPrime.c, partition.c, merge.c etc)

**Symbols** (charCount.c, binarySequence.c)

**Recursion** (factorial.c, gcd.c, lcm.c, fibonacci.c)

**Bitwise operations** (frob.c, bitwise.c, isPowerOf2.c)

**Numeric systems** (base.c, base2.c)

**Pointers** (pointerIncrement.c, func.c)

**Arrays** (arrayUnique.c, lsearch.c, arrayShift.c, arrayMerge.c)

**Strings** (rot13.c, strEqual.c, strReverse.c, strlen.c, strUpperCase.c, strLowerCase.c)

**Matrices** (matrixRotate.c)

**Sorting algorithms** (bubbleSort.c, insertSort.c, selectSort.c, mergeSort.c, quickSort.c)

**Structs** (point.c, vector.c, complex.c)

**Enumerated types** (enum.c)

**Array List** (dynamicArray.c)