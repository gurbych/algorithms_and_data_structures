//Считать с клавиатуры целое число в десятичной системе счисления и основание новой системы счисления (целое число от 2 до 36).
//Вывести в консоль число, записанное в новой системе счисления.
//В качестве цифр, превышающих 9, использовать заглавные буквы латинского алфавита.
//Для решения можно пользоваться только циклами. Задача решается без массивов. Использовать только тип int.

//Пример ввода
//-255 16
//Пример вывода
//-FF

#include <stdio.h>

int main() {
    int initial, base, multiple;
    int max = 1;
    
    scanf("%d %d", &initial, &base);
    
    if ( initial < 0 ) {
        initial *= -1;
        printf("-");
    }
    for ( int i = initial / max; i >= base; ) {
        max *= base;
        i = initial / max;
    }
    for ( ; max > 0; ) {
        multiple = initial / max;
        if ( multiple > 9 ) {
            multiple += 55;
            printf("%c", multiple);
        } else {
            printf("%d", multiple);
        }
        initial %= max;
        max /= base;
    }
    printf("\n");
    
    return 0;
}

