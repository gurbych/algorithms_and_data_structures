//Считать с клавиатуры целое неотрицательное число в десятичной системе счисления и основание новой системы счисления (целое число от 2 до 10).
//Вывести в консоль число, записанное в новой системе счисления.
//Задача решается без массивов.

//Пример ввода
//8 3
//Пример вывода
//22

#include <stdio.h>

int main() {
    int initial, base;
    int max = 1;
    
    scanf("%d %d", &initial, &base);
    
    for ( int i = initial / max; i >= base; ) {
        max *= base;
        i = initial / max;
    }
    for ( ; max > 0; ) {
        printf("%d", initial/max);
        initial %= max;
        max /= base;
    }
    printf("\n");
    
    return 0;
}

