//Вывести число Фибоначчи с заданным целым номером, по модулю не превышающим 46.

#include <stdio.h>

int fibonacci(int n) {
    int prev = 0;
    int current = 1;
    int sum;
    int ifNegative = 0;
    
    if ( n == 1 ) {
        return 1;
    } else if ( n == 0 ) {
        return 0;
    } else if ( n < 0 ) {
        n *= -1;
        ifNegative = 1;
    }
    
    for ( int i = 1; i < n; i++ ) {
        sum = prev + current;
        prev = current;
        current = sum;
    }
    if ( ifNegative && !(n % 2) ) {
        return -1 * current;
    }
    return current;
}

int main() {
    int n;
    
    scanf("%d", &n);
    
    printf("%d\n", fibonacci(n));
    
    return 0;
}

