//Считать с клавиатуры два целых числа.
//Вывести в строку все чётные числа в промежутке от первого до второго включительно.
//В указанном промежутке гарантировано наличие хотя бы одного числа, подлежащего выводу.

//Пример ввода
//7 15
//Пример вывода
//8 10 12 14

#include <stdio.h>

int main() {
    int min, max;
    
    scanf("%d %d", &min, &max);
    if ( min > 0 ) {
        min += min % 2;
    } else {
        min -= min % 2;
    }
    max -= 1;
    for ( ; min < max; min += 2 ) {
        printf("%d ", min);
    }
    printf("%d\n", min);
    
    return 0;
}

