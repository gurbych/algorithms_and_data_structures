//Считать из файла task.in последовательность чисел размером не более 100 элементов.
//Посчитать длину последовательности и вывести ее в task.out.

#include <stdio.h>

int main() {
    FILE *f1 = fopen("task.in", "r");
    FILE *f2 = fopen("task.out", "w");
    int i = 0;
    
    for ( int current = 0; i < 100 && fscanf(f1, "%d", &current) == 1; i++ );
    fprintf(f2, "%d\n", i);
    
    return 0;
}

