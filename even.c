//Считать с клавиатуры целое положительное число.
//Вывести в строку все чётные числа от нуля до указанного числа включительно.

//Пример ввода
//7
//Пример вывода
//0 2 4 6

#include <stdio.h>

int main() {
    int value;
    
    scanf("%d", &value);
    
    if ( value % 2 != 0 ) {
        value -= 1;
    }
    for ( int i = 0; i < value; i += 2 ) {
        printf("%d ", i);
    }
    printf("%d\n", value);
    
    return 0;
}

