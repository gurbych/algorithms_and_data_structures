//Считать min, max. Вывести в столбик кратные пяти числа от min до max включительно.

//Пример ввода
//0 15
//Пример вывода
//0
//5
//10
//15

#include <stdio.h>

#define DIV 5

int main() {
    int min, max, multiple;
    
    scanf("%d %d", &min, &max);
    
    multiple = min - min % DIV;
    if ( multiple < min ) {
        multiple += DIV;
    }
    for ( ; multiple <= max; multiple += DIV ) {
        printf("%d\n", multiple);
    }
    
    return 0;
}

