//Считать с клавиатуры целое число.
//Вывести на экран количество знакомест, нужных для вывода этого числа.

//Пример ввода
//123
//Пример вывода
//3

#include <stdio.h>
 
#define DIV 10
 
int main() {
    int dividend;
    int places = 0;
     
    scanf("%d", &dividend);
     
    if ( dividend <= 0 ) {
        dividend *= -1;
        places += 1;
    }
    for ( int counter = dividend; counter > 0; counter /= DIV ) {
        places += 1;
    }
    printf("%d\n", places);
     
    return 0;
}
