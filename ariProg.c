//Считать с клавиатуры три целых числа - первый член, шаг и количество членов арифметической прогрессии. Вывести в строку через пробел члены заданной прогрессии.
//Задаваемое количество больше нуля.

//Пример ввода
//12 2 4
//Пример вывода
//12 14 16 18

#include <stdio.h>

int main() {
    int start, step, length;
    
    scanf("%d %d %d", &start, &step, &length);
    
    for ( int counter = 1; counter < length; counter++ ) {
        printf("%d ", start);
        start += step;
    }
    
    printf("%d\n", start);
    
    return 0;
}

