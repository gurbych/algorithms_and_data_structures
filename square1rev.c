//Вывести числовой квадрат заданного размера.
//Выведенные числа начинаются с максимального и постоянно уменьшаются.
//В каждой строке числа разделены пробелами.
//Размер считать с клавиатуры.

//Пример ввода
//2
//Пример вывода
//4 3
//2 1

#include <stdio.h>

int main() {
    int total, counter;
    
    scanf("%d", &total);
    counter = total * total;
    for ( int row = total; row > 0; row-- ) {
        for ( int col = 1; col < total; col++ ) {
            printf("%d ", counter);
            counter -= 1;
        }
        printf("%d\n", counter);
        counter -= 1;
    }
    
    return 0;
}

