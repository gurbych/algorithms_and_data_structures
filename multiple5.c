//Считать с клавиатуры целое положительное число.
//Вывести в строку все числа, кратные 5, от нуля до указанного числа включительно.

//Пример ввода
//17
//Пример вывода
//0 5 10 15

#include <stdio.h>

#define MULTIPLIER 5

int main() {
    int integer;
    
    scanf("%d", &integer);
    
    integer -= (integer % MULTIPLIER);
    
    for ( int i = 0; i < integer; i += MULTIPLIER ) {
        printf("%d ", i);
    }
    printf("%d\n", integer);
    
    return 0;
}

