//Считать с клавиатуры 2 целых положительных числа - скорость движения автомобиля и ограничение по скорости. Проверить, нарушает ли водитель автомобиля правило ограничения скорости. Если нарушает, вывести в консоль "violation", если не нарушает — вывести «ok».

//Пример ввода
//60 80
//Пример вывода
//ok

#include <stdio.h>

int main() {
    int currentSpeed, speedLimit;
    
    scanf("%d", &currentSpeed);
    scanf("%d", &speedLimit);
    
    if ( currentSpeed > speedLimit ) {
        printf("violation\n");
    } else {
        printf("ok\n");
    }
    return 0;
}

