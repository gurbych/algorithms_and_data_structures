//структура для хранения комплексных чисел и основные операции с ними

#include <stdio.h>
#include <math.h>

typedef struct {
    double re, im;
} Complex;

void complexIncrement(Complex *this, Complex other) {
    this->re += other.re;
    this->im += other.im;
}

void complexDecrement(Complex *this, Complex other) {
    this->re -= other.re;
    this->im -= other.im;
}

void complexMultiply(Complex *this, Complex other) {
    Complex temp;
    
    temp.re = this->re * other.re - this->im * other.im;
    temp.im = this->im * other.re + this->re * other.im;
    this->re = temp.re;
    this->im = temp.im;
}

Complex complexSum(Complex a, Complex b) {
    Complex sum;
    
    sum.re = a.re + b.re;
    sum.im = a.im + b.im;
    return sum;
}

Complex complexDiff(Complex a, Complex b) {
    Complex dif;
    
    dif.re = a.re - b.re;
    dif.im = a.im - b.im;
    return dif;
}

Complex complexProduct(Complex a, Complex b) {
    Complex product;
    
    product.re = a.re * b.re - a.im * b.im;
    product.im = a.im * b.re + a.re * b.im;
    return product;
}

int complexEqual(Complex a, Complex b) {
    return a.re == b.re && a.im == b.im;
}

double complexAbs(Complex this) {
    return hypot(this.re, this.im);
}

void complexPrint(Complex this) {
    printf("%g%+gi", this.re, this.im);
}

