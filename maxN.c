//Считать с клавиатуры целое положительное число - количество чисел в последовательности.
//Затем считать с клавиатуры заданное количество чисел и вывести наибольшее из них.

//Пример ввода
//5
//1 2 3 4 5
//Пример вывода
//5

#include <stdio.h>

int main() {
    int length, max, next;
    
    scanf("%d %d", &length, &max);
    
    for ( int counter = 1; counter < length; counter++ ) {
        scanf("%d", &next);
        if ( max < next ) {
            max = next;
        }
    }
    printf("%d\n", max);
    
    return 0;
}

