//В файле task.in задана последовательность символов.
//Вывести в task.out количество вхождений латинских букв, которые встречаются в последовательности.
//Большие буквы считать как маленькие.
//Пример ввода
//Hello!
//Пример вывода
//e 1
//h 1
//l 2
//o 1

#include <stdio.h>

#define ABC 26

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    char symbol;
    int abc[ABC];
    
    for ( int i = 0; i < ABC; i++ ) {
        abc[i] = 0;
    }
    
    for ( ; fscanf(in, "%c", &symbol) == 1; ) {
        if ( symbol >= 'A' && symbol <= 'Z' ) {
            abc[symbol-'A'] += 1;
        }
        if ( symbol >= 'a' && symbol <= 'z' ) {
            abc[symbol-'a'] += 1;
        }
    }
    
    for ( int i = 0; i < ABC; i++ ) {
        if ( abc[i] != 0 ) {
            fprintf(out, "%c %d\n", i+'a', abc[i]);
        }
    }
    
    fclose(in);
    fclose(out);
    
    return 0;
}

