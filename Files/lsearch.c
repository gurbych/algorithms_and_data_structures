//В файле task.in дано искомое число needle, а также последовательность чисел размером не более 100 элементов.
//Найти needle в массиве и вывести в файл task.out его индекс либо -1, если число не найдено.

//Пример ввода
//40
//10 20 30 40 50 60 70 80 90 100
//Пример вывода
//3

#include <stdio.h>

#define SIZE 100

int intScan(FILE *in) {
    int current;
    
    fscanf(in, "%d", &current);
    return current;
}

int arrayScan(FILE *in, int array[], int limit) {
    int i = 0;
    
    for ( ; i < limit && fscanf(in, "%d", &array[i]) == 1; i++ );
    return i;
}

int arraySearch(int array[], int size, int needle) {
    for ( int index = 0; index < size; index++ ) {
        if ( array[index] == needle ) {
            return index;
        }
    }
    return -1;
}

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int needle = intScan(in);
    int array[SIZE];
    int length = arrayScan(in, array, SIZE);
    
    fprintf(out, "%d\n", arraySearch(array, length, needle));
    
    fclose(in);
    fclose(out);
    
    return 0;
}

