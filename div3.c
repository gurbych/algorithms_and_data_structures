//Считать min, max. Вывести в столбик кратные тройке числа от min до max включительно.

//Пример ввода
//0 9
//Пример вывода
//0
//3
//6
//9

#include <stdio.h>

#define DIV 3

int main() {
    int min, max, multiple;
    
    scanf("%d %d", &min, &max);
    
    multiple = min - min % DIV;
    if ( multiple < min ) {
        multiple += DIV;
    }
    for ( ; multiple <= max; multiple += DIV ) {
        printf("%d\n", multiple);
    }
    
    return 0;
}

