//Считать с клавиатуры целое положительное число, вывести на экран количество всех делителей этого числа.
//Пример ввода
//12
//Пример вывода
//6

#include <stdio.h>

int main() {
    int divident;
    int counter = 0;
    
    scanf("%d", &divident);
    
    for ( int i = 1; i <= divident; i++ ) {
        if ( divident % i == 0 ) {
            counter += 1;
        }
    }
    printf("%d\n", counter);
    
    return 0;
}

