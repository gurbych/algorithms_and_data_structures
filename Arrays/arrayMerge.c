//Массивы src1[] и src2[] (длиной len1 и len2 соответственно) отсортированы по неубыванию.
//Произвести слияние этих массивов в массив target[], также отсортированный по неубыванию.
//В случае равенства предпочтение отдаётся элементам из первого массива.
//Требуемая сложность: O(N)

#include <stdio.h>

void arrayMerge(int target[], int src1[], int len1, int src2[], int len2) {
    int i = 0;
    int j = 0;
    int k = 0;
    
    for ( ; i < len1 && j < len2; k++ ) {
        if ( src2[j] < src1[i] ) {
            target[k] = src2[j];
            j += 1;
        } else {
            target[k] = src1[i];
            i += 1;
        }
    }
    for ( ; i < len1; i++, k++ ) {
        target[k] = src1[i];
    }
    for ( ; j < len2; j++, k++ ) {
        target[k] = src2[j];
    }
}

void arrayPrint(int array[], int length) {
    for ( int i = 0; i < length; i++ ) {
        printf("%d", array[i]);
    }
}

int main() {
    int src1[] = {0, 1, 2, 3};
    int src2[] = {1, 3, 3, 5, 6, 7};
    int len1 = 4;
    int len2 = 6;
    int len3 = len1 + len2;
    int target[len3];
    
    arrayMerge(target, src1, len1, src2, len2);
    arrayPrint(target, len3);
    
    return 0;
}

