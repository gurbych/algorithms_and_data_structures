//Функция должна вывести элементы массива через пробел, завершив вывод символом перевода строки.

void arrayPrint(FILE *out, int array[], int size) {
    int last = size - 1;
    
    for ( int counter = 0; counter < last; counter++ ) {
        fprintf(out, "%d ", array[counter]);
    }
    fprintf(out, "%d\n", array[last]);
}

