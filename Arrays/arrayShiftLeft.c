//циклический сдвиг массива на 1 элемент вправо

void arrayShiftLeft(int array[], int size) {
    int temp = array[0];
    
    size -= 1;
    for ( int i = 0; i < size; i++ ) {
        array[i] = array[i+1];
    }
    array[size] = temp;
}

