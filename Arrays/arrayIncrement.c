//Увеличить каждый элемент массива на значение increment.

void arrayIncrement(int array[], int size, int increment) {
    for ( int counter = 0; counter < size; counter++ ) {
        array[counter] += increment;
    }
}

