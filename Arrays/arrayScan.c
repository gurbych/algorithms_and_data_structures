//Считать из файла in не более limit элементов в массив array.
//Вернуть количество считанных элементов.

int arrayScan(FILE *in, int array[], int limit) {
    int current = 0;
    
    for ( ; current < limit && fscanf(in, "%d", &array[current]) == 1; current++ );
    return current;
}

