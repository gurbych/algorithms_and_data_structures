//Реализовать циклический сдвиг массива на shift элементов вправо. Максимизировать производительность при оптимальном использовании памяти.
//Требуемая сложность: O(N)

#include <stdio.h>
 
void arrayShift(int array[], int size, int shift) {
    int len;
    //cut extra shift
    shift %= size;
 
    //if shift is less then 0 - redirect shifting left
    if ( shift < 0 ) {
        shift += size;
    }
 
    len = size - shift;
 
    //choosing the algorithm which needs less memory
    if ( shift < len ) {
        //creating temporary array
        int tmpArray[shift];
 
        //filling tmp array
        for ( int i = 0, j = len; i < shift; i++, j++ ) {
            tmpArray[i] = array[j];
        }
 
        //shifting array
        for ( int i = size - 1, j = i - shift; j >= 0; i--, j-- ) {
            array[i] = array[j];
        }
 
        //inserting lost values from tmp array
        for ( int i = 0; i < shift; i++ ) {
            array[i] = tmpArray[i];
        }
    } else {
        //creating temporary array
        int tmpArray[len];
 
        //filling tmp array
        for ( int i = 0; i < len; i++ ) {
            tmpArray[i] = array[i];
        }
 
        //shifting array
        for ( int i = 0, j = len; j < size; i++, j++ ) {
            array[i] = array[j];
        }
 
        //inserting lost values from tmp array
        for ( int i = shift, j = 0; i < size; i++, j++ ) {
            array[i] = tmpArray[j];
        }
    }
}
 
void arrayPrint(int array[], int length) {
    int last = length - 1;
 
    for ( int i = 0; i < last; i++ ) {
        printf("%d_", array[i]);
    }
    printf("%d\n", array[last]);
}
 
int main() {
    int src[] = {1, 2, 3, 4, 5, 6, 7};
    int len = 7;
    int shift = -3;
 
    arrayPrint(src, len);
    arrayShift(src, len, shift);
    arrayPrint(src, len);
 
    return 0;
}

