//Считать в массив последовательность чисел длиной 10 из файла task.in.
//Записать его через пробел в файл task.out.

//Пример ввода
//10 20 30 40 50 60 70 80 90 100
//Пример вывода
//10 20 30 40 50 60 70 80 90 100

#include <stdio.h>

#define SIZE 10
#define LAST 9

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    int array[SIZE];
    
    for ( int counter = 0; counter < SIZE; counter++ ) {
        fscanf(in, "%d", &array[counter]);
    }
    for ( int counter = 0; counter < LAST; counter++ ) {
        fprintf(out, "%d ", array[counter]);
    }
    fprintf(out, "%d\n", array[LAST]);
    fclose(in);
    fclose(out);
    
    return 0;
}

