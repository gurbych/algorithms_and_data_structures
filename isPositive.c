//Считать с клавиатуры число. Если число положительное, вывести на экран «yes», иначе вывести «no».

//Пример ввода
//-20
//Пример вывода
//no

#include <stdio.h>

int main() {
    int a;
    
    scanf("%d", &a);
    
    if ( a <= 0 ) {
        printf("no\n");
    } else {
        printf("yes\n");
    }
    
    return 0;
}

