//Считать с клавиатуры целое положительное число.
//Вывести в строку через пробел по порядку все натуральные числа, не превышающие заданное.
//Пример ввода
//5
//Пример вывода
//1 2 3 4 5

#include <stdio.h>

int main() {
    int natural;
    
    scanf("%d", &natural);
    
    for ( int i = 1; i < natural; i++ ) {
        printf("%d ", i);
    }
    printf("%d\n", natural);
    
    return 0;
}

