//Считать с клавиатуры целое неотрицательное число. Вывести через пробел степени числа 2 от нулевой до заданной. Использовать цикл.

//Пример ввода
//3
//Пример вывода
//1 2 4 8

#include <stdio.h>

#define BIN 2

int main() {
    int exponent;
    int power = 1;
    
    scanf("%d", &exponent);
    
    for ( int i = 0; i < exponent; i++ ) {
        printf("%d ", power);
        power *= BIN;
    }
    printf("%d\n", power);
    
    return 0;
}

