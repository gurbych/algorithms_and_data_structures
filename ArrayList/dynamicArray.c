#include <stdio.h>
#include <stdlib.h>

enum {
    SIZE = 10
} size;

typedef struct {
    int* array;
    int maxSize;
    int currentSize;
} List;

void initialize(List* lst, int size) {
    lst->array = (int*)malloc(sizeof(int)*size);
    lst->maxSize = size;
    lst->currentSize = 0;
}

void fill(List* lst, int value) {
    for(int i = 0; i < lst->maxSize; i++) {
        lst->array[i] = value;
    }
    lst->currentSize = lst->maxSize;
}

void printList(List* lst) {
    int last = lst->currentSize - 1;

    for(int i = 0; i < last; i++) {
        printf("%d ", lst->array[i]);
    }
    printf("%d\n", lst->array[last]);
}

void changeSize(List* lst, int amount) {
    int newSize = lst->maxSize + amount;

    lst->array = (int*)realloc(lst->array, sizeof(int)*newSize);
    lst->maxSize += amount;
    printf("Size has been changed!\n");
}

void add(List* lst, int value, int times) {
    int availableSize = lst->maxSize - lst->currentSize;
    int addSize = times - availableSize;

    if (addSize > 0) {
        changeSize(lst, addSize);
    }

    for(int i = lst->currentSize; i < lst->maxSize; i++, lst->currentSize++) {
        lst->array[i] = value;
    }
}

void delete(List* lst) {
    free(lst->array);
    lst->array = 0;
    lst->maxSize = 0;
    lst->currentSize = 0;
    printf("Goodbye!\n");
}

int main() {
    List myList;

    initialize(&myList, SIZE);
    fill(&myList, 13);
    add(&myList, 666, 5);
    printList(&myList);
    printf("--------------------\n");
    printf("%d\n", myList.currentSize);
    printf("%d\n", myList.maxSize);
    delete(&myList);

    return 0;
}
