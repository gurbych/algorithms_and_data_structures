//В файле task.in дана последовательность символов – нулей и единиц.
//Вывести в task.out минимально необходимое число обменов пар значений для упорядочивания последовательности по неубыванию.
//Пример ввода
//1101
//Пример вывода
//1

#include <stdio.h>

int main() {
    FILE *in = fopen("task.in", "r");
    FILE *out = fopen("task.out", "w");
    FILE *in2 = fopen("task.in", "r");
    int current;
    int zeros = 0;
    int rearrangments = 0;
    
    for ( ; fscanf(in, "%1d", &current) == 1; ) {
        printf("%d", current);
        if ( current == 0 ) {
            zeros += 1;
        }
    }
    printf("\n%d\n", zeros);
    fclose(in);    
    for ( int i = 0; i < zeros; i++ ) {
        fscanf(in2, "%1d", &current);
        if ( current == 1 ) {
            rearrangments += 1;
        }
    }
    printf("%d\n", rearrangments);
    
    fprintf(out, "%d\n", rearrangments);
    
    fclose(in2);
    fclose(out);
    
    return 0;
}

