//поворот матрицы на 90 градусов по часовой стрелке

void matrixRotate90(int target[SIZE][SIZE], int source[SIZE][SIZE], int size) {
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            int newRow = size - row - 1;
             
            target[newRow][col] = source[col][row];
        }
    }
}
