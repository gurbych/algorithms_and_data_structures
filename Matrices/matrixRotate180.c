//Поворот матрицы на 180 градусов по часовой стрелке.

void matrixRotate180(int target[SIZE][SIZE], int source[SIZE][SIZE], int size) {
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            int newRow = size - row - 1;
            int newCol = size - col - 1;
             
            target[newRow][newCol] = source[row][col];
        }
    }
}
