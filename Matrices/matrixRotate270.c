//Поворот матрицы на 270 градусов по часовой стрелке.

void matrixRotate270(int target[SIZE][SIZE], int source[SIZE][SIZE], int size) {
    for ( int row = 0; row < size; row++ ) {
        for ( int col = 0; col < size; col++ ) {
            int newCol = size - col - 1;
             
            target[row][newCol] = source[col][row];
        }
    }
}
