// функция выводит матрицу в файл

void matrixPrint(FILE *out, int matrix[SIZE][SIZE], int size) {
    int last = size - 1;
    
    for ( int i = 0; i < size; i++ ) {
        for ( int j = 0; j < last; j++ ) {
            fprintf(out, "%d ", matrix[j][i]);
        }
        fprintf(out, "%d\n", matrix[last][i]);
    }
}

