#include <stdio.h>

int main() {
    int matrix[10][10];
    int counter = 0;

    for (int col = 0; col < 10; col++) {
        for (int row = 0; row < 10; row++) {
            matrix[row][col] = counter;
            counter += 1;
        }
    }

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 9; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("%d\n", matrix[i][9]);
    }

    return 0;
}
